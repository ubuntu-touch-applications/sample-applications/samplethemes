/*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * sampleThemes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'samplethemes.aloysliska'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    
    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Sample Themes')
            
            contents: Item { 
                id: mainHeaderText
                anchors.fill: parent
                Label {
                    id: labelTitle
                    text: i18n.tr('Sample Themes v1.0')
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: units.gu(1)
                }
                
                Item {  // use item to center Label between labelTitle and mainHeaderActionBar
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: labelTitle.right
                    anchors.right: parent.right
                    height: parent.height
                    
                    Label {
                        width: parent.width
                        height: parent.height
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        
                        text: {
                            if (Theme.name == "Lomiri.Components.Themes.Ambiance")
                                return "Ambiance"
                            else if (Theme.name == "Lomiri.Components.Themes.SuruDark")
                                return "SuruDark"
                            else
                                return Theme.name
                        }
                    }
                }
            }                
            
            trailingActionBar {
                id: mainHeaderActionBar
                actions: [   
                    Action {
                        iconName: "display-brightness-max"
                        text: i18n.tr('Ambiance theme')
                        onTriggered: {
                            Theme.name = "Lomiri.Components.Themes.Ambiance"
                        }   
                    },
                    Action {
                        iconName: "display-brightness-min"
                        text: i18n.tr('SuruDark theme')
                        onTriggered: {
                            Theme.name = "Lomiri.Components.Themes.SuruDark"
                        }
                    }
                ]
            }
        }            

        Flickable {
            anchors.top: header.bottom
            width: parent.width
            height: parent.height - header.height
            contentHeight: rectColumn.height
        
            Column {
                id: rectColumn
                width: parent.width
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.activity
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('activityText in activity')
                        color: theme.palette.normal.activityText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.background
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Column {
                        anchors.fill: parent
                        topPadding: units.gu(2)
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('backgroundText in background')
                            color: theme.palette.normal.backgroundText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('backgroundSecondaryText in background')
                            color: theme.palette.normal.backgroundSecondaryText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('backgroundTertiaryText in background')
                            color: theme.palette.normal.backgroundTertiaryText
                            horizontalAlignment: Label.AlignHCenter                    
                        }    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.base
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('baseText in base')
                        color: theme.palette.normal.baseText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.field
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('fieldText in field')
                        color: theme.palette.normal.fieldText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.focus
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('focusText in focus')
                        color: theme.palette.normal.focusText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.foreground
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('foregroundText in foreground')
                        color: theme.palette.normal.foregroundText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.negative
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('negativeText in negative')
                        color: theme.palette.normal.negativeText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.overlay
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Column {
                        anchors.fill: parent
                        topPadding: units.gu(2)
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('overlayext in overlay')
                            color: theme.palette.normal.overlayText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('overlaySecondaryText in overlay')
                            color: theme.palette.normal.overlaySecondaryText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.position
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('positionText in position')
                        color: theme.palette.normal.positionText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.positive
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('positiveText in positive')
                        color: theme.palette.normal.positiveText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }

                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.raised
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Column {
                        anchors.fill: parent
                        topPadding: units.gu(2)
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('raisedText in raised')
                            color: theme.palette.normal.raisedText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                        
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr('raisedSecondaryText in raised')
                            color: theme.palette.normal.raisedSecondaryText
                            horizontalAlignment: Label.AlignHCenter                    
                        }
                    }
                }
                
                Rectangle {
                    height: units.gu(10)
                    width: parent.width
                    color: theme.palette.normal.selection
                    border.color: theme.palette.normal.background
                    border.width: 5
                    radius: 10
                    
                    Label {
                        anchors.centerIn: parent
                        text: i18n.tr('selectionText in selection')
                        color: theme.palette.normal.selectionText
                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter                    
                    }
                }
            }
        }
    }
}
